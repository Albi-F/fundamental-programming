#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

name = input("What's your name? ")
first_number = int(input("What's your first number? "))
second_number = int(input("What's your second number? "))

sum = first_number + second_number

print("name: ", name)
print(first_number, " + ", second_number, " = ", sum)
