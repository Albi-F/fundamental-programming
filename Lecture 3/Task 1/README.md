```mermaid
graph TD;
    A([Start])
    A --> B[/Print 'Hi there' to the screen/]
    B --> C[Create variables for first and last name]
    C --> D[/Print full name to screen/]
    D --> E([End])
```
