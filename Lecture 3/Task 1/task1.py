#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# Gitlab: https://gitlab.com/Albi-F/

print("Hi there!")

first_name = "Alberto"
last_name = "Fabbri"

print(first_name, last_name)
print(first_name + " " + last_name)
