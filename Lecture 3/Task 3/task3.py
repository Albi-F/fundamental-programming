#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

first_number = 5
second_number = 3
third_number = 4.7
radius = 8.345
PI = 3.14

circumference = PI * (radius * 2)

print(first_number, "\t+\t", second_number, "\t=\t", first_number + second_number)

print(first_number, "\t+\t", third_number, "\t=\t", first_number + third_number)

print("The circumference is approximately ", format(circumference, ".1f"))
