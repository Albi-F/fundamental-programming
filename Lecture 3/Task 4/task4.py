#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

from datetime import date

import logging

logging.basicConfig(level=logging.DEBUG)

first_name = input("Please enter your first name... ")
last_name = input("Please enter your last name... ")

# Checking that it is a number
while True:
    try:
        age = int(input("Please enter your age... "))
        break
    except ValueError:
        print("Your age must be a number")

phone_number = input("Please enter your phone number... ")

current_year = date.today().year
logging.debug("This is the year %s", current_year)

birthyear = current_year - age

print(
    first_name,
    last_name,
    "was born in",
    birthyear,
    "and can be contacted at",
    phone_number,
)
