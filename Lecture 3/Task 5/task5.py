#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import logging

logging.basicConfig(level=logging.DEBUG)

# This is to be able to check if an exception has been thrown
# because in that case 'distance' keeps the 'None' value
distance = None
# Common error text
zero_value_error = "0 is not a valid value"
while True:
    try:
        # Check that 'distance' is an integer different from '0'
        if (distance is None) or (distance == 0):
            distance = int(input("Please insert the distance [km]... "))
            # I want the value only when it has been changed
            logging.debug(f"Distance value: {distance}")
        if distance == 0:
            raise ValueError(zero_value_error)

        # Check that 'average_speed' is an integer different from '0'
        average_speed = int(input("Please insert the average speed [km/h]... "))
        logging.debug("Average speed value: %s", average_speed)
        if average_speed == 0:
            raise ValueError(zero_value_error)
        else:
            break
    except ValueError:
        print("Please insert an integer with a not zero value :)")

total_time_required_in_minutes = (distance / average_speed) * 60

hours_required = int(total_time_required_in_minutes // 60)

minutes_minus_hours_required = int(total_time_required_in_minutes % 60)

print(
    "Your travel will last about",
    hours_required,
    "hours and",
    minutes_minus_hours_required,
    "minutes.",
)
