```mermaid
graph TD;
    A([Start])
    A --> B[/Insert distance/]
    B --> C{Is it a number?}
    C -- No --> B
    C -- Yes --> D{Is it different from 0?}
    D -- No --> B
    D -- Yes --> E[/Insert average speed/]
    E --> F{Is it a number?}
    F -- No --> D
    F -- Yes --> G{Is it different from 0?}
    G -- No --> D
    G -- Yes --> H[Calculate time]
    H --> I[/Print time/]
    I --> J([End])
```