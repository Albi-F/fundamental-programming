#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import re

while True:
    money_string = input("Insert money value [kr]: ")
    if re.search(r"^[0-9]+\.?[0-9]+$", money_string):
        present_value = float(money_string)
        break
    else:
        print("Please write a number")

while True:
    years_string = input("Insert number of years: ")
    if years_string.isdigit():
        number_of_years = int(years_string)
        break

INTEREST_RATE = 0.05

future_value = present_value * (1 + INTEREST_RATE) ** number_of_years

print(
    "After",
    number_of_years,
    "you will have",
    format(future_value, ".2f"),
    "swedish kronas",
)
