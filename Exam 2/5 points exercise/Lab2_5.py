#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93


import sys
import logging


logging.basicConfig(level=logging.INFO)


def read_file(filename: str) -> list:
    """
    Reads all the numbers in the specified file and
    adds them to a list as integers.

    Args:
        filename (str): Path to the file

    Returns:
        list: List of integers
    """
    try:
        with open(filename, "r") as file:
            integer_list = [int(integer.rstrip("\n"))
                            for line in file
                            for integer in line.split(" ")]
    except FileNotFoundError:
        exit("The file '" + filename + "' can't be opened")
    except ValueError:
        exit(str(filename) + " contains non-integer values")
    else:
        logging.debug("Integer list from file: " + str(integer_list))
        return integer_list


def filter_odd_or_even(numbers: list, odd: bool) -> list:
    """
    Creates a new list that is filled with either the odd or even
    numbers from the parameter list depending on the odd parameter.

    Args:
        numbers (list): Source list of numbers
        odd (bool): True for odd numbers, false for even numbers

    Returns:
        list: A list of even or odd numbers
    """
    return [num for num in numbers
            if num % 2 == 0 and not odd or num % 2 != 0 and odd]


def reversed_bubble_sort(numbers: list) -> None:
    """
    Takes a list of integer numbers as parameter and sorts it in place.

    Args:
        numbers (list): List to sort
    """
    swapped = True
    while swapped is True:
        swapped = False
        for index in range(1, len(numbers)):
            logging.debug("Index: " + str(index) + ", is "
                          + str(numbers[index-1]) + " > "
                          + str(numbers[index]) + "?")
            if numbers[index-1] < numbers[index]:
                logging.debug("Yes!")
                numbers[index-1], numbers[index] = numbers[index], numbers[index-1]
                logging.debug("Reverse bubble after:\t\t" + str(numbers))
                swapped = True


def main() -> None:
    """
    Reads two files from the command line,
    extract the odd numbers from the first file and the even numbers
    from the second one. Than it joins the lists and sort it with
    the reversed bubble sort algorithm.
    """
    try:
        file_paths = sys.argv[1:3]
    except IndexError:
        exit("It is necessary to give two files as arguments")
    else:
        complete_integer_list = []
        odd = True
        for file in file_paths:
            filtered_integer_list = filter_odd_or_even(read_file(file), odd)
            odd = not odd
            complete_integer_list += filtered_integer_list
        logging.debug("Complete list:\t\t\t" + str(complete_integer_list))
        reversed_bubble_sort(complete_integer_list)
        logging.debug("Complete list sorted:\t\t" + str(complete_integer_list))
        print(complete_integer_list)


if __name__ == "__main__":
    main()
