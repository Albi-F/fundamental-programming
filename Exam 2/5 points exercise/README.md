```mermaid
graph TD;
Start([Start])
Start --> Read_files[/Read files from cmd and<br>save the numbers into lists/]
Read_files --> Pick_numbers_from_lists[Pick even or odd numbers from the lists<br> and save them in new lists]
Pick_numbers_from_lists --> Combine_lists[Combine the even and odd<br>numbers lists together]
Combine_lists --> Sort_list[Sort the combined list in reverse order]
Sort_list --> Print_list[Print sorted list]
Print_list --> End([End])
```