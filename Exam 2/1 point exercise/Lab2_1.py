#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import sys
import logging

# Only the debug level is used
logging.basicConfig(level=logging.INFO)


def read_file(file_path: str) -> list:
    """
    Read a file with names and messages and return it as a list

    Args:
        file_path (str): File to open

    Returns:
        list: List with tuples made of name + message
    """
    try:
        with open(file_path, "r") as log_file:
            messages = []
            while True:
                # It is possible to read as many lines as wanted from
                # an empty file without getting any exception
                name = log_file.readline().rstrip("\n")
                text = log_file.readline().rstrip("\n")
                if not name or not text:
                    break
                message = (name, text)
                messages.append(message)
            return messages
    except FileNotFoundError:
        print("Error: The file '" + file_path + "' could not be found.")
        exit()


def display_entry(name: str, message: str) -> None:
    """
    Print the string [name] --> message

    Args:
        name (str): name of a person
        message (str): message to print
    """
    print("[", name, "] --> ", message, sep="")


def main() -> None:
    """
    Search inside a file specified as a command line argument
    all messages sent by a person whose name is asked at runtime
    """
    logging.debug("System arguments list " + str(sys.argv))
    try:
        file_path = sys.argv[1]
    except IndexError:
        print("Error: No parameter provided")
        exit()
    else:
        # Trying to read the file before asking for the name
        # because the file path can be invalid
        messages = read_file(file_path)
        name = ""
        while name == "":
            name = input("Enter a name to search for: ")
        for message in messages:
            # Uppercase and lowercase letters make different names
            if message[0] == name:
                display_entry(message[0], message[1])


if __name__ == "__main__":
    main()
