```mermaid
graph TD;
Start([Start])
Start --> Read_file_name[/Read file name from<br>command line arguments/]
Read_file_name --> Is_file_name_valid?{Is file name valid?}
Is_file_name_valid? -- No --> Print_file_name_error_message[/ERROR:<br>File name is invalid/]
Is_file_name_valid? -- Yes --> Read_user_name[/Enter user name/]
Read_user_name --> Open_file[Open txt file]
Open_file --> Read_lines[Read two new lines]
Read_lines -->EOF?{End of file?}
EOF? -- No --> Save_name_message[Save name and message<br>in new tuple]
Save_name_message --> Read_lines
EOF? -- Yes --> Print_tuples[/"Print all tuples<br>(name + message)"/]
Print_tuples --> End([End])
```