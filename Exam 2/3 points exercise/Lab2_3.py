#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import sys
import logging


logging.basicConfig(level=logging.INFO)


def read_lines(filename: str) -> list:
    """
    Read all the lines from a file, strip them of the new line
    character, and place them in a list that is returned

    Args:
        filename (str): path of the file to open

    Returns:
        list: list with all the row of the file
        stripped of new line characters
    """
    try:
        with open(filename, "r") as cars_file:
            cars_strings_list = []
            for line in cars_file:
                cars_strings_list.append(line.rstrip("\n"))
            return cars_strings_list
    except FileNotFoundError:
        print("An error occurred while trying to read the file.")
        exit()


def parse_cars(list_of_strings: list) -> list:
    """
    Convert a list of strings in a list of tuples

    Args:
        list_of_strings (list): strings made of model_name:range

    Returns:
        list: list of tuples (model_name, range)
    """
    cars_range = []
    for row, string in enumerate(list_of_strings, start=1):
        try:
            # This line can trigger ValueError too if
            # there are no ':' in the string
            (model, range) = string.split(":")
            range = int(range)
            cars_range.append((model, range))
        except ValueError:
            exit("There is something wrong on line " + str(row))
    return cars_range


def calculate_percentage(distance: int, cars: list) -> list:
    """
    Calculates the range as a percentage of the distance

    Args:
        distance (int): distance used to calculate the percentage
        cars (list): list of tuples (model_name, range)

    Returns:
        list: list of tuples (model_name, percentage)
    """
    cars_percentage = []
    for tuple in cars:
        (model, range) = tuple
        percentage = (distance*100)/range
        cars_percentage.append((model, percentage))
    return cars_percentage


def display_result(percentages: list) -> None:
    """
    Display a header and a list of tuples on the screen

    Args:
        percentages (list): list to print
    """
    print("""\nTo drive the specified distance would correspond to this many
percent of each cars specified max range.""")
    # Debug
    first_col_width = max(len(percentage[0]) for percentage in percentages)
    logging.debug("N° characters longest car model: " + str(first_col_width))
    for line in percentages:
        (model, percentage) = line
        print(format(model, "<36"), end=" ")
        percentage_string = str(round(percentage)) + "%"
        if percentage > 100:
            percentage_string = "Distance exceeds max range (" + str(percentage_string) + ")"
        print("--> ", percentage_string)


def main() -> None:
    """
    Display a list of cars with their range shown as percentage
    of the distance given in input
    """
    try:
        file_path = sys.argv[1]
    except IndexError:
        exit("Error: No parameter provided")
    else:
        cars_strings_list = read_lines(file_path)
        cars_range = parse_cars(cars_strings_list)
        while True:
            try:
                distance = int(input("\nHow far do you want to drive (kilometers)? "))
                if distance <= 0:
                    print("0 and negative numbers are not allowed")
                else:
                    break
            except ValueError:
                print("You should enter an integer")
        cars_percentage = calculate_percentage(distance, cars_range)
        display_result(cars_percentage)


if __name__ == "__main__":
    main()
