```mermaid
graph TD;
Start([Start])
Start --> Read_input_file[/Read input file/]
Read_input_file --> Save_data_to_list["Save data from file<br>into a list of tuples<br>('Car', range)"]
Save_data_to_list --> Ask_travel_distance[/Ask user what distance<br>he/she wants to travel/]
Ask_travel_distance --> Calculate_range_percentages[For each car calculate and save in a<br>list what is the percentage of charge<br>required to cover that distance]
Calculate_range_percentages --> Display_cars_percentages[/Print list of cars + percentage required/]
Display_cars_percentages --> End([End])
```