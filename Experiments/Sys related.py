#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import sys

print("\nAll names defined by module:")
for item in dir(sys):
    print(item)

print("\nPYTHONPATH:")
for item in sys.path:
    print(item)


variable1 = 1  # this variable is global


def function1():
    variable2 = "Hi"
    print("\nGlobal variables function1:")
    for item in globals():
        print(item)
    print("\nLocal variables function1:")
    for item in locals():
        print(item)
    return variable2


print("\nGlobal variables module:")
for item in globals():
    print(item)
print("\nLocal variables module:")
for item in locals():
    print(item)

print()
function1()

print()
