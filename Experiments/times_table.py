# Print times table

# f-string notation
for y in range(1, 11):
    for x in range(1, 11):
        print(f"{x * y:>4}", end="")
    print()

# format function
for y in range(1, 11):
    for x in range(1, 11):
        print(format(x * y, "4"), end="")
    print()
