#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

from Lab1_5 import (
    from_xyMath_to_xyLabyrinth,
    from_xyLabyrinth_to_xyMath,
)  # The code to test
import pytest  # Needed for parametrize


# Testing with multiple inputs
@pytest.mark.parametrize("xyMath, xyLabyrinth", [((1, 2), (0, 8)), ((8, 8), (7, 2))])
def test_from_xyMath_to_xyLabyrinth(xyMath, xyLabyrinth):
    assert from_xyMath_to_xyLabyrinth(xyMath[0], xyMath[1]) == (
        xyLabyrinth[0],
        xyLabyrinth[1],
    )


# Testing with just one input
def test_from_xyLabyrinth_to_xyMath():
    assert from_xyLabyrinth_to_xyMath(0, 8) == (1, 2)
