```mermaid
graph TD;
start([Start])
start --> input[/Ask player where to move/]
input --> requested_position[Calculate requested position]
requested_position --> requested_position_valid?{Is the requested<br>position valid?}
requested_position_valid? -- No --> invalid_requested_position[Print wall message]
invalid_requested_position --> input
requested_position_valid? -- Yes --> read_map[Read position from map]
read_map --> victory?{Has the player<br>reached the end?}
victory? -- Yes --> stop([End])
victory? --No --> do_according_to_map[Do what is written on the map]
do_according_to_map --> input
```