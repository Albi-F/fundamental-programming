#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import logging
import copy

# noqa is used to disable flake8 errors
# Black directive --> disabled
# fmt: off

# AREPL
# Run out of moves
standard_input1 = ["d","d","w","w","s","s","a","a","w","w","w","w","w","w","a","a","s","s","s","s"]  # noqa: E231, E501

# Possible to reach left trap
standard_input2 = ["w","w","w","w","w","w","a","a","s","s","s","s","s","s","s","s","d","d","d","w"]   # noqa: E231, E501

# Prevent going outside board
standard_input3 = ["w","w","w","w","w","w","a","a","a","s","a","s","a","s","a","s","a","s","a","s","a","s","a","s","s","d","s","d","s","d","w"]  # noqa: E231, E501

# Reach exit using top power-up
standard_input4 = ["w","w","w","w","d","d","d","w","w","a","d","s","s","d","s","s","s","s","s","d","d","d","w","w","w","w","a","w","w","w"]  # noqa: E231, E501

# Reach exit using right power-up
standard_input5 = ["w","w","w","w","d","d","d","d","s","s","s","s","s","d","d","d","w","w","a","d","w","w","a","w","w","w"]  # noqa: E231, E501

# Top power - Top Trap - Top power - Exit
standard_input6 = ["w","w","w","w","d","d","d","w","w","a","d","d","w","w","w","w","d","d","d","w","w","a","d","s","s","d","s","s","s","s","s","s","d","d","d","w","w","w","w","a","w","w","w"]  # noqa: E231, E501

# Black directive --> enabled
# fmt: on

# Keep debugging stuff separated from output
logging.basicConfig(level=logging.WARNING)

# Dictionary with all player data
player_data = {
    "moves_left": 20,  # Initial moves
    "position_x": None,
    "position_y": None,
}

# Moves gained when you get a power-up
POWER_UP_MOVES_GAINED = 15

# Messages printed to the player
messages = {
    "trap": "Oh no, a trap!\n",
    "power_up": "A chocolate bar, I feel stronger\n",
    "wall": "Ouch! I can not walk through walls...\n",
    "victory": "You survived! Well done adventurer!\n",
    "defeat": "Game over! You did not reach the exit in time.\n",
}

# Symbols used to print the map
symbols = {
    "wall": "@",
    "free": " ",
    "star": "S",
    "end ": "E",
    "p_up": "P",
    "trap": "T",
    "player": "*",
}


# Python uses negative index to get elements starting from the right side of a list
# Defining a subclass of the list class and redefining __getitem__ to raise an exception for negative indexes.
class mylist(list):
    def __getitem__(self, n):
        if n < 0:
            raise IndexError("Index lower than 0")
        return list.__getitem__(self, n)


# Tell black to not format this list
# fmt: off
labyrinth = mylist([   # O.O
    mylist([symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"]]),  # noqa: E501
    mylist([symbols["free"], symbols["free"], symbols["free"], symbols["wall"], symbols["p_up"], symbols["free"], symbols["trap"], symbols["wall"], symbols["end "], symbols["wall"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["wall"], symbols["free"], symbols["wall"], symbols["wall"], symbols["free"], symbols["trap"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["free"], symbols["free"], symbols["free"], symbols["free"], symbols["free"], symbols["wall"], symbols["free"], symbols["wall"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["free"], symbols["wall"], symbols["free"], symbols["free"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["wall"], symbols["free"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["p_up"], symbols["free"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["star"], symbols["free"], symbols["free"], symbols["wall"], symbols["free"], symbols["wall"], symbols["wall"], symbols["free"]]),  # noqa: E501
    mylist([symbols["free"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["free"], symbols["free"], symbols["free"], symbols["free"]]),  # noqa: E501
    mylist([symbols["free"], symbols["free"], symbols["free"], symbols["trap"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"], symbols["wall"]]),  # noqa: E501
])
# fmt: on

# xyLabyrinth coordinates start in the upper left corner with value (0,0)
# and they increase going down or to the right

# xyMath coordinates start in the lower left corner with value (1,1)
# and they increase going up and to the right

# All of this has been done to print coordinates in a more human readable way


# Transform coordinates from math to labyrinth system
def from_xyMath_to_xyLabyrinth(position_xMath, position_yMath):
    position_xLabyrinth = position_xMath - 1
    position_yLabyrinth = 10 - position_yMath
    return (position_xLabyrinth, position_yLabyrinth)


# Transform coordinates from labyrinth to math system
def from_xyLabyrinth_to_xyMath(position_xLabyrinth, position_yLabyrinth):
    position_xMath = position_xLabyrinth + 1
    position_yMath = 10 - position_yLabyrinth
    return (position_xMath, position_yMath)


# Write a symbol to a labyrinth
def write_symbol_to_labyrinth_position(
    position_xMath, position_yMath, symbol, labyrinth
):
    position_xyLabyrinth = from_xyMath_to_xyLabyrinth(position_xMath, position_yMath)
    logging.debug("-------------------")
    logging.debug(
        "Writing symbol "
        + symbol
        + " to labyrinth in the position xLabyrinth: "
        + str(position_xyLabyrinth[0])
        + ", yLabyrinth: "
        + str(position_xyLabyrinth[1])
    )
    # The first index is the position y because it selects the nested list
    labyrinth[position_xyLabyrinth[1]][position_xyLabyrinth[0]] = symbol


# Read the symbol at the given position of the labyrinth
# If the indexes are invalid it raise an exception
def read_symbol_from_labyrinth_position(position_xMath, position_yMath, labyrinth):
    try:
        position_xyLabyrinth = from_xyMath_to_xyLabyrinth(
            position_xMath, position_yMath
        )
        symbol = labyrinth[position_xyLabyrinth[1]][position_xyLabyrinth[0]]
        return symbol
    except IndexError:
        raise


# Look for a symbol in the labyrinth, it returns the first one
def read_symbol_position_from_labyrinth(symbol, labyrinth):
    for column in labyrinth:
        try:
            position_xLabyrinth = column.index(symbol)
            position_yLabyrinth = labyrinth.index(column)
            position_xyMath = from_xyLabyrinth_to_xyMath(
                position_xLabyrinth, position_yLabyrinth
            )
            return position_xyMath
        except ValueError:
            pass


# Logging Info - Print labyrinth
def print_labyrinth():
    dinamic_labyrinth = copy.deepcopy(labyrinth)
    if player_data["position_x"] is not None and player_data["position_y"] is not None:
        write_symbol_to_labyrinth_position(
            player_data["position_x"],
            player_data["position_y"],
            symbols["player"],
            dinamic_labyrinth,
        )
    logging.info("--------------------")
    for data in player_data:
        logging.info(data + ":" + str(player_data[data]))
    logging.info("--------------------")
    for column in dinamic_labyrinth:
        logging.info(column)


# Update starting position
def move_user_to_starting_point():
    starting_point = read_symbol_position_from_labyrinth(symbols["star"], labyrinth)
    player_data["position_x"] = starting_point[0]
    player_data["position_y"] = starting_point[1]
    print_labyrinth()


# Used to save the position where the user would like to go
requested_position = {
    "position_x": None,
    "position_y": None,
}


# Avoid execution if imported, see below
def main():
    # Logging info - Initial labyrinth
    print_labyrinth()
    move_user_to_starting_point()
    print()
    print(
        "You have been placed in a pitch-black labyrinth,",
        "without knowing if there is a way out or not.",
        "Maybe there are traps? The only option available",
        "is to walk in a random direction and hope for the best,",
        "hope that you do not walk into a wall, or even worse,",
        "that you walk in circles.",
        "But hurry up, you only have so many moves… ",
    )
    print()
    print(
        """
w --> up
a --> left
s --> down
d --> right
        """
    )
    while True:
        # Reset requested position
        requested_position["position_x"] = player_data["position_x"]
        requested_position["position_y"] = player_data["position_y"]
        # Ask for player input
        direction = input("\nEnter direction: ")
        if direction == "w":
            requested_position["position_y"] = player_data["position_y"] + 1
        elif direction == "a":
            requested_position["position_x"] = player_data["position_x"] - 1
        elif direction == "s":
            requested_position["position_y"] = player_data["position_y"] - 1
        elif direction == "d":
            requested_position["position_x"] = player_data["position_x"] + 1
        else:
            # if the input is invalid start a new iteration
            print("\nInvalid command!")
            continue
        logging.debug(
            "\nRequested position, xMath: "
            + str(requested_position["position_x"])
            + ", yMath: "
            + str(requested_position["position_y"])
        )
        try:
            # Try to read what is present in the position requested by the user
            requested_position_status = read_symbol_from_labyrinth_position(
                requested_position["position_x"],
                requested_position["position_y"],
                labyrinth,
            )
            logging.debug(
                "The symbol of the requested position is: '"
                + requested_position_status
                + "'"
            )
            # Wall
            if requested_position_status == symbols["wall"]:
                print_labyrinth()
                print(messages["wall"])
            # Trap
            elif requested_position_status == symbols["trap"]:
                print_labyrinth()
                print(messages["trap"])
                player_data["moves_left"] -= 1
                # Remove the trap from the map
                # write_symbol_to_labyrinth_position(
                #     requested_position["position_x"],
                #     requested_position["position_y"],
                #     symbols["free"],
                #     labyrinth,
                # )
                move_user_to_starting_point()
            else:
                player_data["position_x"] = requested_position["position_x"]
                player_data["position_y"] = requested_position["position_y"]
                player_data["moves_left"] -= 1
                print_labyrinth()
                # Victory
                if requested_position_status == symbols["end "]:
                    print(messages["victory"])
                    break
                # Power-up
                if requested_position_status == symbols["p_up"]:
                    print(messages["power_up"])
                    player_data["moves_left"] += POWER_UP_MOVES_GAINED
                    # Remove the power-up from the map
                    # write_symbol_to_labyrinth_position(
                    #     requested_position["position_x"],
                    #     requested_position["position_y"],
                    #     symbols["free"],
                    #     labyrinth,
                    # )
            # Defeat
            if player_data["moves_left"] == 0:
                print_labyrinth()
                print(messages["defeat"])
                break
        except IndexError:
            # The user is trying to go outside of the labyrinth
            print_labyrinth()
            print(messages["wall"])
            logging.debug("Trying to go outside of the labyrinth...")


# Execute the program if not imported
if __name__ == "__main__":
    main()
