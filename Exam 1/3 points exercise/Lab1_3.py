#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

# Debugging with AREPL
standard_input = ["seventy", "70", "220"]

bags_dimension = {"big": 80, "medium": 50, "small": 20}

bags_value = {"big": 60000, "medium": 30000, "small": 10000}

# One big bag is mandatory as the minimum dimension of the truck is 100
bags_necessary = {"big": 1, "medium": 0, "small": 0}

# Welcome the user
print("Welcome to the Money Bag Transport Calculator (M.B.T.C.")
print("-------------------------------------------------------")

# Get the dimension of the truck from the user
while True:
    try:
        truck_volume = int(input("\nWhat is the volume of the truck (>= 100L): "))
        if truck_volume >= 100:
            break
        else:
            print("\nThe number it too low")
    except ValueError:
        print("\nYou have to insert an integer number")

# I want to keep in memory the dimension of the truck even if is not required
space_left = truck_volume

# Remove the mandatory first big bag
space_left -= bags_dimension["big"]

# Calculating which bags to use
for bag_dimension in bags_dimension:
    while space_left >= bags_dimension[bag_dimension]:
        space_left -= bags_dimension[bag_dimension]
        bags_necessary[bag_dimension] += 1
total_value = 0
# Calculating the total value of the bags
for bag_necessary in bags_necessary:
    total_value += bags_value[bag_necessary] * bags_necessary[bag_necessary]

# Final output
print("\nPacking plan")
print("------------")
for bag_necessary in bags_necessary:
    print(bags_necessary[bag_necessary], bag_necessary, "bags")

print("\nSpace left:", space_left)
print("Truck volume", truck_volume)
print("Total value: ", total_value, "kr", sep="")
