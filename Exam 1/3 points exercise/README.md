```mermaid
graph TD;
Start([Start])
Start --> User_input[/Insert truck volume/]
User_input --> Check_volume{Is total truck<br>volume >= 100?}
Check_volume -- No --> User_input
Check_volume -- Yes --> Check_bag_size_available{Should >= 1 bag size<br>be counted?}
Check_bag_size_available -- Yes --> Set_bag_dimension[bag = next biggest<br>bag not yet counted]
Set_bag_dimension --> Check_bag_fit{Is bag volume <=<br>free truck volume?}
Check_bag_fit -- No --> Check_bag_size_available
Check_bag_fit -- Yes --> Add_bag[Add 1 bag of that dimension]
Add_bag --> Remove_volume[remove its volume from the free truck volume]
Remove_volume --> Check_bag_fit
Check_bag_size_available -- No --> Print_output[/Print output/]
Print_output --> End([End])
```