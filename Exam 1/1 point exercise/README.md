Generic flow chart:
```mermaid
graph TD;
Start([Start])
Start --> Ask_ticket_type[/"Ask for ticket type [1-3]"/]
Ask_ticket_type --> Save_ticket[Save ticket type]
Save_ticket --> Ask_menu_option{"Ask for menu option [1-5]"}
Ask_menu_option -- 1-4 --> option_1-4[Add/remove meal/bag]
option_1-4 --> Ask_menu_option
Ask_menu_option -- 5 --> option_5[Print receipt]
option_5 --> End([End])
```

Full version flow chart:

```mermaid
graph TD;
Start([Start])
Start --> Ask_ticket_type[/"Ask for ticket type [1-3]"/]
Ask_ticket_type --> C{Is it a number?}
C -- No --> Ask_ticket_type
C -- Yes --> D{Is it >= 1 and <= 3?}
D -- No --> Ask_ticket_type
D -- Yes --> E[Save ticket type]
E --> Ask_which_option[/"Ask for menu option [1-5]"/]
Ask_which_option --> G{Is it a number?}
G -- No --> Ask_which_option
G -- Yes --> H{"Have the user pressed 1?<br>[Add a bag]"}
H -- Yes --> I{Do the user already have a bag?}
I -- No --> J[Add a bag]
J --> Ask_which_option
I -- Yes --> K[/You can't have more the one bag/]
K --> Ask_which_option
H -- No --> L{"Have the user pressed 2?<br>[Add a meal]"}
L -- Yes --> M{Do the user already have a meal?}
M -- No --> N[Add a meal]
N --> Ask_which_option
M -- Yes --> O[/You can't have more the one meal/]
O --> Ask_which_option
L -- No --> P{"Have the user pressed 3?<br>[Remove a bag]"}
P -- Yes --> Q{Do the user have a bag?}
Q -- Yes --> R[Remove a bag]
R --> Ask_which_option
Q -- No --> S[/You don't have a bag/]
S --> Ask_which_option
P -- No --> T{"Have the user pressed 4?<br>[Remove a meal]"}
T -- Yes --> U{Do the user have a meal?}
U -- Yes --> V[Remove a meal]
V --> Ask_which_option
U -- No --> W[/You don't have a meal/]
W --> Ask_which_option
T -- No --> X{"Have the user pressed 5?<br>[Go to receipt]"}
X -- No --> Y[/You choice is invalid/]
Y --> Ask_which_option
X -- Yes --> Z[/Print receipt/]
Z --> End([End])
```
SVG version [here](https://mermaid.ink/svg/eyJjb2RlIjoiZ3JhcGggVEQ7XG5TdGFydChbU3RhcnRdKVxuU3RhcnQgLS0-IEFza190aWNrZXRfdHlwZVsvXCJBc2sgZm9yIHRpY2tldCB0eXBlIFsxLTNdXCIvXVxuQXNrX3RpY2tldF90eXBlIC0tPiBDe0lzIGl0IGEgbnVtYmVyP31cbkMgLS0gTm8gLS0-IEFza190aWNrZXRfdHlwZVxuQyAtLSBZZXMgLS0-IER7SXMgaXQgPj0gMSBhbmQgPD0gMz99XG5EIC0tIE5vIC0tPiBBc2tfdGlja2V0X3R5cGVcbkQgLS0gWWVzIC0tPiBFW1NhdmUgdGlja2V0IHR5cGVdXG5FIC0tPiBBc2tfd2hpY2hfb3B0aW9uWy9cIkFzayBmb3IgbWVudSBvcHRpb24gWzEtNV1cIi9dXG5Bc2tfd2hpY2hfb3B0aW9uIC0tPiBHe0lzIGl0IGEgbnVtYmVyP31cbkcgLS0gTm8gLS0-IEFza193aGljaF9vcHRpb25cbkcgLS0gWWVzIC0tPiBIe1wiSGF2ZSB0aGUgdXNlciBwcmVzc2VkIDE_PGJyPltBZGQgYSBiYWddXCJ9XG5IIC0tIFllcyAtLT4gSXtEbyB0aGUgdXNlciBhbHJlYWR5IGhhdmUgYSBiYWc_fVxuSSAtLSBObyAtLT4gSltBZGQgYSBiYWddXG5KIC0tPiBBc2tfd2hpY2hfb3B0aW9uXG5JIC0tIFllcyAtLT4gS1svWW91IGNhbid0IGhhdmUgbW9yZSB0aGUgb25lIGJhZy9dXG5LIC0tPiBBc2tfd2hpY2hfb3B0aW9uXG5IIC0tIE5vIC0tPiBMe1wiSGF2ZSB0aGUgdXNlciBwcmVzc2VkIDI_PGJyPltBZGQgYSBtZWFsXVwifVxuTCAtLSBZZXMgLS0-IE17RG8gdGhlIHVzZXIgYWxyZWFkeSBoYXZlIGEgbWVhbD99XG5NIC0tIE5vIC0tPiBOW0FkZCBhIG1lYWxdXG5OIC0tPiBBc2tfd2hpY2hfb3B0aW9uXG5NIC0tIFllcyAtLT4gT1svWW91IGNhbid0IGhhdmUgbW9yZSB0aGUgb25lIG1lYWwvXVxuTyAtLT4gQXNrX3doaWNoX29wdGlvblxuTCAtLSBObyAtLT4gUHtcIkhhdmUgdGhlIHVzZXIgcHJlc3NlZCAzPzxicj5bUmVtb3ZlIGEgYmFnXVwifVxuUCAtLSBZZXMgLS0-IFF7RG8gdGhlIHVzZXIgaGF2ZSBhIGJhZz99XG5RIC0tIFllcyAtLT4gUltSZW1vdmUgYSBiYWddXG5SIC0tPiBBc2tfd2hpY2hfb3B0aW9uXG5RIC0tIE5vIC0tPiBTWy9Zb3UgZG9uJ3QgaGF2ZSBhIGJhZy9dXG5TIC0tPiBBc2tfd2hpY2hfb3B0aW9uXG5QIC0tIE5vIC0tPiBUe1wiSGF2ZSB0aGUgdXNlciBwcmVzc2VkIDQ_PGJyPltSZW1vdmUgYSBtZWFsXVwifVxuVCAtLSBZZXMgLS0-IFV7RG8gdGhlIHVzZXIgaGF2ZSBhIG1lYWw_fVxuVSAtLSBZZXMgLS0-IFZbUmVtb3ZlIGEgbWVhbF1cblYgLS0-IEFza193aGljaF9vcHRpb25cblUgLS0gTm8gLS0-IFdbL1lvdSBkb24ndCBoYXZlIGEgbWVhbC9dXG5XIC0tPiBBc2tfd2hpY2hfb3B0aW9uXG5UIC0tIE5vIC0tPiBYe1wiSGF2ZSB0aGUgdXNlciBwcmVzc2VkIDU_PGJyPltHbyB0byByZWNlaXB0XVwifVxuWCAtLSBObyAtLT4gWVsvWW91IGNob2ljZSBpcyBpbnZhbGlkL11cblkgLS0-IEFza193aGljaF9vcHRpb25cblggLS0gWWVzIC0tPiBaWy9QcmludCByZWNlaXB0L11cblogLS0-IEVuZChbRW5kXSkiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCIsInRoZW1lVmFyaWFibGVzIjp7ImJhY2tncm91bmQiOiJ3aGl0ZSIsInByaW1hcnlDb2xvciI6IiNFQ0VDRkYiLCJzZWNvbmRhcnlDb2xvciI6IiNmZmZmZGUiLCJ0ZXJ0aWFyeUNvbG9yIjoiaHNsKDgwLCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJwcmltYXJ5Qm9yZGVyQ29sb3IiOiJoc2woMjQwLCA2MCUsIDg2LjI3NDUwOTgwMzklKSIsInNlY29uZGFyeUJvcmRlckNvbG9yIjoiaHNsKDYwLCA2MCUsIDgzLjUyOTQxMTc2NDclKSIsInRlcnRpYXJ5Qm9yZGVyQ29sb3IiOiJoc2woODAsIDYwJSwgODYuMjc0NTA5ODAzOSUpIiwicHJpbWFyeVRleHRDb2xvciI6IiMxMzEzMDAiLCJzZWNvbmRhcnlUZXh0Q29sb3IiOiIjMDAwMDIxIiwidGVydGlhcnlUZXh0Q29sb3IiOiJyZ2IoOS41MDAwMDAwMDAxLCA5LjUwMDAwMDAwMDEsIDkuNTAwMDAwMDAwMSkiLCJsaW5lQ29sb3IiOiIjMzMzMzMzIiwidGV4dENvbG9yIjoiIzMzMyIsIm1haW5Ca2ciOiIjRUNFQ0ZGIiwic2Vjb25kQmtnIjoiI2ZmZmZkZSIsImJvcmRlcjEiOiIjOTM3MERCIiwiYm9yZGVyMiI6IiNhYWFhMzMiLCJhcnJvd2hlYWRDb2xvciI6IiMzMzMzMzMiLCJmb250RmFtaWx5IjoiXCJ0cmVidWNoZXQgbXNcIiwgdmVyZGFuYSwgYXJpYWwiLCJmb250U2l6ZSI6IjE2cHgiLCJsYWJlbEJhY2tncm91bmQiOiIjZThlOGU4Iiwibm9kZUJrZyI6IiNFQ0VDRkYiLCJub2RlQm9yZGVyIjoiIzkzNzBEQiIsImNsdXN0ZXJCa2ciOiIjZmZmZmRlIiwiY2x1c3RlckJvcmRlciI6IiNhYWFhMzMiLCJkZWZhdWx0TGlua0NvbG9yIjoiIzMzMzMzMyIsInRpdGxlQ29sb3IiOiIjMzMzIiwiZWRnZUxhYmVsQmFja2dyb3VuZCI6IiNlOGU4ZTgiLCJhY3RvckJvcmRlciI6ImhzbCgyNTkuNjI2MTY4MjI0MywgNTkuNzc2NTM2MzEyOCUsIDg3LjkwMTk2MDc4NDMlKSIsImFjdG9yQmtnIjoiI0VDRUNGRiIsImFjdG9yVGV4dENvbG9yIjoiYmxhY2siLCJhY3RvckxpbmVDb2xvciI6ImdyZXkiLCJzaWduYWxDb2xvciI6IiMzMzMiLCJzaWduYWxUZXh0Q29sb3IiOiIjMzMzIiwibGFiZWxCb3hCa2dDb2xvciI6IiNFQ0VDRkYiLCJsYWJlbEJveEJvcmRlckNvbG9yIjoiaHNsKDI1OS42MjYxNjgyMjQzLCA1OS43NzY1MzYzMTI4JSwgODcuOTAxOTYwNzg0MyUpIiwibGFiZWxUZXh0Q29sb3IiOiJibGFjayIsImxvb3BUZXh0Q29sb3IiOiJibGFjayIsIm5vdGVCb3JkZXJDb2xvciI6IiNhYWFhMzMiLCJub3RlQmtnQ29sb3IiOiIjZmZmNWFkIiwibm90ZVRleHRDb2xvciI6ImJsYWNrIiwiYWN0aXZhdGlvbkJvcmRlckNvbG9yIjoiIzY2NiIsImFjdGl2YXRpb25Ca2dDb2xvciI6IiNmNGY0ZjQiLCJzZXF1ZW5jZU51bWJlckNvbG9yIjoid2hpdGUiLCJzZWN0aW9uQmtnQ29sb3IiOiJyZ2JhKDEwMiwgMTAyLCAyNTUsIDAuNDkpIiwiYWx0U2VjdGlvbkJrZ0NvbG9yIjoid2hpdGUiLCJzZWN0aW9uQmtnQ29sb3IyIjoiI2ZmZjQwMCIsInRhc2tCb3JkZXJDb2xvciI6IiM1MzRmYmMiLCJ0YXNrQmtnQ29sb3IiOiIjOGE5MGRkIiwidGFza1RleHRMaWdodENvbG9yIjoid2hpdGUiLCJ0YXNrVGV4dENvbG9yIjoid2hpdGUiLCJ0YXNrVGV4dERhcmtDb2xvciI6ImJsYWNrIiwidGFza1RleHRPdXRzaWRlQ29sb3IiOiJibGFjayIsInRhc2tUZXh0Q2xpY2thYmxlQ29sb3IiOiIjMDAzMTYzIiwiYWN0aXZlVGFza0JvcmRlckNvbG9yIjoiIzUzNGZiYyIsImFjdGl2ZVRhc2tCa2dDb2xvciI6IiNiZmM3ZmYiLCJncmlkQ29sb3IiOiJsaWdodGdyZXkiLCJkb25lVGFza0JrZ0NvbG9yIjoibGlnaHRncmV5IiwiZG9uZVRhc2tCb3JkZXJDb2xvciI6ImdyZXkiLCJjcml0Qm9yZGVyQ29sb3IiOiIjZmY4ODg4IiwiY3JpdEJrZ0NvbG9yIjoicmVkIiwidG9kYXlMaW5lQ29sb3IiOiJyZWQiLCJsYWJlbENvbG9yIjoiYmxhY2siLCJlcnJvckJrZ0NvbG9yIjoiIzU1MjIyMiIsImVycm9yVGV4dENvbG9yIjoiIzU1MjIyMiIsImNsYXNzVGV4dCI6IiMxMzEzMDAiLCJmaWxsVHlwZTAiOiIjRUNFQ0ZGIiwiZmlsbFR5cGUxIjoiI2ZmZmZkZSIsImZpbGxUeXBlMiI6ImhzbCgzMDQsIDEwMCUsIDk2LjI3NDUwOTgwMzklKSIsImZpbGxUeXBlMyI6ImhzbCgxMjQsIDEwMCUsIDkzLjUyOTQxMTc2NDclKSIsImZpbGxUeXBlNCI6ImhzbCgxNzYsIDEwMCUsIDk2LjI3NDUwOTgwMzklKSIsImZpbGxUeXBlNSI6ImhzbCgtNCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIiwiZmlsbFR5cGU2IjoiaHNsKDgsIDEwMCUsIDk2LjI3NDUwOTgwMzklKSIsImZpbGxUeXBlNyI6ImhzbCgxODgsIDEwMCUsIDkzLjUyOTQxMTc2NDclKSJ9fSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)