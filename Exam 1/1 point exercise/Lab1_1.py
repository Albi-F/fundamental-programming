#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

# Separate "normal output" from debug stuff
import logging

logging.basicConfig(level=logging.INFO)

# Debugging with AREPL
standard_input = [1, 5]

# Common warning message for the user
not_a_digit_string_error = "\nYou should write a digit!"

# Dictionary where to save the user choices
user_choices = {"Ticket": None, "Bag": False, "Meal": False}

# Dictionary with the types of tickets
ticket_types = {
    "Budget": 500,  # swedish kronas
    "Economy": 750,  # swedish kronas
    "VIP": 2000,  # swedish kronas
}

# Tuple with the menu options
options = (
    "Add bag (max 1)",
    "Add meal (max 1)",
    "Remove bag",
    "Remove meal",
    "Finalize ticket",
)

BAG_PRICE = 200  # swedish kronas

MEAL_PRICE = 150  # swedish kronas

# Ask the user to choose a type of ticket
while True:
    try:
        # Text more readable
        print()
        # Using the ticket types dictionary to show the user the various options
        for count, ticket in enumerate(ticket_types, 1):
            print(
                count,
                ") ",
                "{:<8}".format(ticket),
                ":\t",
                ticket_types[ticket],
                "kr",
                sep="",
            )
        # Reading the standard input
        ticket_number = int(input("Write the digit of ticket you want to buy... "))
        # If I get here it means that the user wrote an integer, otherwise an exception would have been thrown
        # The user made a valid choice
        if ticket_number >= 1 and ticket_number <= 3:
            # The user wants this type of ticket
            ticket_desidered = list(ticket_types)[ticket_number - 1]
            logging.debug(f"The user chosed this ticket: {ticket_desidered}")
            # Saving the ticket
            user_choices["Ticket"] = ticket_desidered
            break
        else:
            # The user wrote an invalid number
            print("\nThe digit should be >= 1 and <= 3!")
    # The user input was a string or a floating number
    except ValueError:
        print(not_a_digit_string_error)


# Show the user the menu with all the options and do what he asks
while True:
    try:
        # Show the user his/her current shopping cart
        print("\nYour current shopping cart is: ")
        for choice in user_choices:
            print("{:<8}".format(choice), ":\t", user_choices[choice], sep="")
        # A void line to make the output more readable
        print()
        # Show the menu
        for count, option in enumerate(options, 1):
            print(count, ") ", option, sep="")
        # Ask the user for input
        option_chosen = int(input("Any option? Write the digit of the option... "))

        # Add bag
        if option_chosen == 1:
            if user_choices["Bag"] is True:
                print("\nYou already have a bag in your shopping cart!")
            else:
                user_choices["Bag"] = True
                print("\nYou have added a bag to your shopping cart!")

        # Add meal
        elif option_chosen == 2:
            if user_choices["Meal"] is True:
                print("\nYou already have a meal in your shopping cart!")
            else:
                user_choices["Meal"] = True
                print("\nYou have added a meal to your shopping cart!")

        # Remove bag
        elif option_chosen == 3:
            if user_choices["Bag"] is False:
                print("\nYou don't have a bag in your shopping cart!")
            else:
                user_choices["Bag"] = False
                print("\nYour bag has been removed from your shopping cart!")

        # Remove meal
        elif option_chosen == 4:
            if user_choices["Meal"] is False:
                print("\nYou don't have a meal in your shopping cart!")
            else:
                user_choices["Meal"] = False
                print("\nYour meal has been removed from your shopping cart!")
        # Go to receipt
        elif option_chosen == 5:
            break
        else:
            print("\nThe digit should be >= 1 and <= 5!")
    # user wrote an invalid integer
    except ValueError:
        print(not_a_digit_string_error)

user_bag_price = 0
user_meal_price = 0
print("\nReceipt:")
for choice in user_choices:

    # First row: ticket type + price
    if choice == "Ticket":
        print(
            "{:<7}".format(choice),
            ":",
            "{:>5}".format(ticket_types[user_choices[choice]]),
            "kr",
            sep="",
        )

    # Second row: bag + yes or no
    elif choice == "Bag":
        if user_choices[choice] is True:
            user_bag_price = BAG_PRICE
            print(
                "{:<7}".format(choice),
                ":",
                "{:>5}".format(user_bag_price),
                "kr",
                sep="",
            )

    # Third row: meal + yes or no
    elif choice == "Meal":
        if user_choices[choice] is True:
            user_meal_price = MEAL_PRICE
            print(
                "{:<7}".format(choice),
                ":",
                "{:>5}".format(user_meal_price),
                "kr",
                sep="",
            )


print("{:>15}".format("-------"))
# Total
total = int(ticket_types[user_choices["Ticket"]]) + user_bag_price + user_meal_price
print("{:<7}".format("Total") + ":" + "{:>5}".format(total), "kr", sep="")
