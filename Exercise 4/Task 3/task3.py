# Fibonacci Sequence


def print_fiboncci_sequence_with_loop(count):
    print("\nFibonacci sequence calculated with a loop")
    first_number = 0
    print("Number [0]:", first_number)
    second_number = 1
    print("Number [1]:", second_number)
    count -= 2
    for index in range(count):
        next_value = first_number + second_number
        first_number = second_number
        second_number = next_value
        print("Number [", index, "]: ", second_number, sep="")


def print_fiboncci_sequence_with_recursive_function(count):
    print("\nFibonacci sequence calculated with a recursive function")
    first_number = 0
    print("Number [0]:", first_number)
    second_number = 1
    print("Number [1]:", second_number)
    index = 2
    fibonacci(0, 1, count, index)


def fibonacci(num1, num2, count, index):
    next_value = num1 + num2
    first_number = num2
    second_number = next_value
    index += 1
    print("Number [", index, "]: ", second_number, sep="")
    if index != count:
        fibonacci(first_number, second_number, count, index)


def main():
    print_fiboncci_sequence_with_loop(5)
    print_fiboncci_sequence_with_recursive_function(5)


if __name__ == "__main__":
    main()
