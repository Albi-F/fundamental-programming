def find_smallest_number(num_list):
    smallest_number = num_list[0]
    for num in num_list[1:]:
        if num < smallest_number:
            smallest_number = num
    return smallest_number


def find_biggest_number(num_list):
    biggest_number = num_list[0]
    for num in num_list[1:]:
        if num > biggest_number:
            biggest_number = num
    return biggest_number


def calculate_average_number(num_list):
    average_number = 0.0
    for num in num_list:
        average_number += num
    average_number /= len(num_list)
    return average_number


def calculate_sum(num_list):
    sum = 0
    for num in num_list:
        sum += num
    return sum


def getting_numbers_from_user():
    num_list = []
    escape_character = "n"
    user_input = None
    while str(user_input) != escape_character:
        user_input = input(
            "\nInsert a number or '" + escape_character + "' to escape stop: "
        )
        if user_input == escape_character:
            return num_list
        try:
            num_list.insert(0, float(user_input))
        except ValueError:
            print("The value inserted is not valid")


def main():
    num_list = getting_numbers_from_user()
    print("The smallest number is:", find_smallest_number(num_list))
    print("The biggest number is:", find_biggest_number(num_list))
    print("The average number is:", calculate_average_number(num_list))
    print("The sum is:", calculate_sum(num_list))


if __name__ == "__main__":
    main()
