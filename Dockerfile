# The FROM instruction specifies the Parent Image from which you are building
FROM python:3.9-slim-buster

# Avoid error while installing python packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    build-essential \
    python3-dev \
    git \
    gnupg2

# The WORKDIR command is used to define the working directory of a Docker container at any given time
# Any RUN, CMD, ADD, COPY, or ENTRYPOINT command will be executed in the specified working directory
WORKDIR /app

# Install pip requirements in Python virtual environments
ENV VIRTUAL_ENV=.fp_venv
RUN python -m venv $VIRTUAL_ENV
ADD requirements.txt .
RUN $VIRTUAL_ENV/bin/pip install -r requirements.txt

# While similar, ADD and COPY are actually different commands
# COPY is the simplest of the two, since it just copies a file or a directory from your host to your image
# ADD does this too, but also has some more magical features like extracting TAR files or fetching files from remote URLs
ADD . /app

# Switching to a non-root user, please refer to https://aka.ms/vscode-docker-python-user-rights
# RUN useradd appuser && chown -R appuser /app
# USER appuser

# --------------------------------------------
# EXPOSE and ENV are cheap commands to run. If you bust the cache for them, rebuilding them is almost instantaneous.
# Therefore, it’s best to declare these commands as late as possible.
# You should only ever declare ENVs whenever you need them in your build process.

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1
# --------------------------------------------