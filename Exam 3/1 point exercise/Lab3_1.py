#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import sys
import pickle


THRESHOLD = 2


def read_file(filename: str) -> dict:
    try:
        with open(filename, "rb") as file:
            return pickle.load(file)
    except FileNotFoundError:
        print("Error: The files given as arguments are not valid.")
        exit()


def map_to_int(measurements: dict) -> dict:
    try:
        return {location: int(temperature.strip("°"))
                for location, temperature in measurements.items()}
    except ValueError:
        exit("Error parsing temperatures")


def find_faulty(primary: dict, secondary: dict, threshold: int) -> set:
    try:
        return {location for (location, temperature) in primary.items()
                if abs(temperature - secondary[location]) > threshold}
    except KeyError:
        exit("The locations are different")


def display_warnings(faulty_sensors: set) -> None:
    header = """Analyzis of the provided files is complete.
-------------------------------------------

The following sensors differ more than 2°:"""
    print(header, *(location for location in faulty_sensors), sep="\n")


def main() -> None:
    try:
        display_warnings(find_faulty(
            *[map_to_int(read_file(file)) for file in sys.argv[1:3]],
            THRESHOLD))
    # Python doesn't raise IndexError when reading a range from a list
    # In case no args is given on the cmd line, sys.argv[1:3] returns an empty array
    # Thus the function find_faulty() raises an error because its parameters are wrong
    except TypeError:
        exit("Two arguments required")


if __name__ == "__main__":
    main()
