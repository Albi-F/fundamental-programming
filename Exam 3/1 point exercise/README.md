```mermaid
graph TD;
start([Start]) --> check_cmd_args{Is there another arg in cmd line?}
check_cmd_args -- True --> dictionary_from_file[Return dictionary from file]
dictionary_from_file --> map_to_file[Create new dictionary with integer values]
map_to_file --> check_cmd_args
check_cmd_args -- False --> find_faulty[Find faulty sensors]
find_faulty --> print_warnings[/Print warnings/]
print_warnings --> stop([Stop])
```