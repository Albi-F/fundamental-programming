#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import sys
import pickle

# Avoid using exit() and quit() as they are only available
# if the site module is installed.

# Be careful when using sys.exit() as,
# if is given a value different than 0,
# it raises the exception SystemExit.
# The same happens with exit() and quit()

# Avoid using os._exit() as it exits the program
# without calling cleanup handlers, flushing stdio buffers, etc.


def display_menu() -> None:
    print("""1. Add file
2. Calculate""")


def cross_reference(files: list) -> set:
    try:
        set_list = []
        for file in files:
            with open(file, "r") as telephone_numbers:
                set_list.append({line.strip() for line in telephone_numbers})
        return set.intersection(*set_list)
    except FileNotFoundError:
        print("Error: There was a problem with at least one of the files.")
        sys.exit(0)
    except TypeError:
        print("Error: No file has been given")
        sys.exit(0)


def map_numbers_to_names(numbers: set, filename: str) -> list:
    try:
        suspects = []
        with open(filename, "rb") as file:
            people = pickle.loads(file.read())
            for number in numbers:
                if number in people.keys():
                    for key, value in people.items():
                        if number == key:
                            suspects.append(value)
                            break
                else:
                    suspects.append("Unknown (" + number + ")")
        return suspects
    except FileNotFoundError:
        print("Error: The argument passed on cmd line is not a valid file name.")
        sys.exit(0)


def display_suspects(names: list) -> None:
    header = """The following persons was present on all crime scenes:
------------------------------------------------------"""
    print(header, *(name for name in names), sep="\n")


def main() -> None:
    # Checking immediately if the required argument is there
    # If not the program terminate without wasting time
    try:
        contacts_file_path = sys.argv[1]
    except IndexError:
        print("Error: one argument on cmd line required")
    else:
        files_paths_list = []
        while (True):
            display_menu()
            chosen_option = input("Choose option: ")
            try:
                if chosen_option == "1":
                    files_paths_list.append(input("File path: "))
                elif chosen_option == "2":
                    break
                else:
                    print("Invalid choice")
            # If the user press CTRL + Z ignore EOF and asks for input again
            except EOFError:
                pass
        display_suspects(map_numbers_to_names(
            (cross_reference(files_paths_list)),
            contacts_file_path))


if __name__ == "__main__":
    main()
