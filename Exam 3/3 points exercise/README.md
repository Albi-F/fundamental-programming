```mermaid
graph TD;
start([Start]) --> add_another_file?{Do you want to add another<br>file with numbers?}
add_another_file? -- True --> get_file_name[/Get file name/]
get_file_name --> add_file[Add file to list]
add_file --> add_another_file?
add_another_file? -- False --> create_numbers_set[create a set with shared numbers]
create_numbers_set --> get_dict_from_cmd[/"Read (numbers: name) dictionary from cmd"/]
get_dict_from_cmd --> lookup_for_names[Look up for the names<br>of the owners of the telephone numbers]
lookup_for_names --> save_names_to_list[Save the names to a list]
save_names_to_list --> print_names[/Display names/]
print_names --> stop([Stop])
```