#     _    _ _     _
#    / \  | | |__ (_)
#   / _ \ | | '_ \| |
#  / ___ \| | |_) | |
# /_/   \_\_|_.__/|_|
# Author: Alberto Fabbri
# GitLab: https://gitlab.com/Albi-F/
# GitHub: https://github.com/AlbertoFabbri93

import sys
import pickle
from typing import List, Set

TRANSLATION = {
    "0": "",
    "1": "",
    "2": "abc",
    "3": "def",
    "4": "ghi",
    "5": "jkl",
    "6": "mno",
    "7": "pqrs",
    "8": "tuv",
    "9": "wxyz"
}


def read_orders(filename: str) -> Set[str]:
    try:
        with open(filename, "rb") as file:
            return pickle.loads(file.read())
    except FileNotFoundError:
        print("Error: There was a problem with at least one of the files.")
        sys.exit(0)


def read_words(filename: str) -> List[str]:
    try:
        with open(filename, "r") as file:
            return [word.strip() for word in file]
    except FileNotFoundError:
        print("Error: There was a problem with at least one of the files.")
        sys.exit(0)


def find_all_possible_combinations(order_number: str) -> List[str]:
    combinations: List[str] = [""]
    for digit in order_number:
        combinations = add_digit(digit, combinations)
    return combinations


def add_digit(digit: str, combinations: List[str]) -> List[str]:
    return [combination + char
            for combination in combinations
            for char in TRANSLATION[digit]]


def filter_valid_words(possible_combinations: List[str],
                       valid_words: List[str]) -> List[str]:
    return [combination
            for combination in possible_combinations
            if combination in valid_words]


def display_possible_words(order_number: str, words: List[str]) -> None:
    # This function doesn't fail if the words array is empty
    print(order_number, ": ", end="")
    if not words:
        print("no words found")
    else:
        for index, word in enumerate(words):
            if index == 0:
                print(words[0])
            else:
                print(word.rjust(13))


def main() -> None:
    try:
        orders: Set[str] = read_orders(sys.argv[1])
        words: List[str] = read_words(sys.argv[2])
    except IndexError:
        print("Error: two arguments required")
    else:
        for order in orders:
            possible_combinations: List[str] = find_all_possible_combinations(order)
            valid_words: List[str] = filter_valid_words(possible_combinations, words)
            display_possible_words(order, valid_words)


if __name__ == "__main__":
    main()
