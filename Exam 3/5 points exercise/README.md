```mermaid
graph TD;
start([Start])
--> read_numbers_file[/Read numbers file from cmd line arg/]
--> read_words_file[/Read words file from cmd line arg/] 
--> find_combinations[Find all combinations for every number]
--> find_valid_word[Find valid word comparing the combinations with the words]
--> print_valid_words[/Print valid words/]
--> stop([Stop])
```